import operator

from flask import Flask, jsonify, request, url_for
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

DATASTORE = []


@app.route('/', methods=('DELETE',))
def clear():
    DATASTORE[:] = []
    return jsonify(DATASTORE)


@app.route('/', methods=('POST',))
def create():
    todo = request.get_json()
    todo['completed'] = False
    todo['url'] = url_for('view', id=len(DATASTORE), _external=True)
    DATASTORE.append(todo)
    return jsonify(**todo)


@app.route('/<int:id>', methods=('DELETE',))
def delete(id):
    DATASTORE[id] = None
    return jsonify(DATASTORE[id])


@app.route('/<int:id>', methods=('PATCH',))
def edit(id):
    todo = DATASTORE[id]
    todo.update(**request.get_json())
    return jsonify(todo)


@app.route('/')
def list(list=list):
    return jsonify(list(filter(operator.truth, DATASTORE)))


@app.route('/<int:id>')
def view(id):
    return jsonify(**DATASTORE[id])


if __name__ == '__main__':
    app.run(debug=True, host='localhost')
